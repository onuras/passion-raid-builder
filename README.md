# Passion Raid Builder

### Usage

1. Create a bot here: https://discord.com/developers/applications
2. Make sure you select: "Server Members Intent" and "Message content Intent" in Bot tab for your application.
3. Add bot to your discord server.
4. Make sure bot can read messages in the channels you are going to use signs up. You can test it like this:
    ```
    # Test getting member list
    curl -H 'Authorization: Bot YOUR_BOT_TOKEN' 'https://discord.com/api/guilds/YOUR_SERVER_ID/members?limit=1000'

    # Test getting messages
    curl -H 'Authorization: Bot YOUR_BOT_TOKEN' 'https://discord.com/api/channels/YOUR_CHANNEL_ID/messages'
    ```
5. Run the app with:
    ```
    docker run -d --name passion-raid-builder \
        -v /tmp/database_path_in_your_local_machine:/db \
        -e DATABASE_PATH=/db/db.sqlite3 \
        -e BOT_TOKEN=YOUR_BOT_TOKEN \
        -e GUILD_ID=YOUR_DISCORD_SERVER_ID \
        -e CHANNELS=YOUR_CHANNEL_ID#YOUR_CHANNEL_NAME,YOUR_SECOND_CHANNEL_ID#YOUR_SECOND_CHANNEL_NAME \
        -p 8080:8080 \
        registry.gitlab.com/onuras/passion-raid-builder
    ```
6. It should be accessible on: http://localhost:8080
