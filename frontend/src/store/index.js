import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    members: [],
    raid: {},
    raids: [],
    channels: [],
    specTexts: [
      {
        id: 0,
        name: 'Death Knight: Blood (Tank)',
      },
      {
        id: 1,
        name: 'Death Knight: Blood (DPS)',
      },
      {
        id: 2,
        name: 'Death Knight: Frost (Tank)',
      },
      {
        id: 3,
        name: 'Death Knight: Frost (DPS)',
      },
      {
        id: 4,
        name: 'Death Knight: Unholy',
      },
      {
        id: 5,
        name: 'Druid: Balance',
      },
      {
        id: 6,
        name: 'Druid: Bear',
      },
      {
        id: 7,
        name: 'Druid: Cat',
      },
      {
        id: 8,
        name: 'Druid: Restoration',
      },
      {
        id: 9,
        name: 'Hunter: Beast Mastery',
      },
      {
        id: 10,
        name: 'Hunter: Marksmanship',
      },
      {
        id: 11,
        name: 'Hunter: Survival',
      },
      {
        id: 12,
        name: 'Mage: Arcane',
      },
      {
        id: 13,
        name: 'Mage: Fire',
      },
      {
        id: 14,
        name: 'Mage: Frost',
      },
      {
        id: 15,
        name: 'Paladin: Holy',
      },
      {
        id: 16,
        name: 'Paladin: Protection',
      },
      {
        id: 17,
        name: 'Paladin: Retribution',
      },
      {
        id: 18,
        name: 'Priest: Discipline',
      },
      {
        id: 19,
        name: 'Priest: Holy',
      },
      {
        id: 20,
        name: 'Priest: Shadow',
      },
      {
        id: 21,
        name: 'Rogue: Assasination',
      },
      {
        id: 22,
        name: 'Rogue: Combat',
      },
      {
        id: 23,
        name: 'Rogue: Subtlety',
      },
      {
        id: 24,
        name: 'Shaman: Elemental',
      },
      {
        id: 25,
        name: 'Shaman: Enhancement',
      },
      {
        id: 26,
        name: 'Shaman: Restoration',
      },
      {
        id: 27,
        name: 'Warlock: Affliction',
      },
      {
        id: 28,
        name: 'Warlock: Demonology',
      },
      {
        id: 29,
        name: 'Warlock: Destruction',
      },
      {
        id: 30,
        name: 'Warrior: Arms',
      },
      {
        id: 31,
        name: 'Warrior: Fury',
      },
      {
        id: 32,
        name: 'Warrior: Protection',
      },
    ],
  },
  getters: {
    name: (state) => (id) => state.members.find((m) => m.id === id).nick || state.members.find((m) => m.id === id).username,
    rank: (state) => (id) => state.members.find((m) => m.id === id).rank,
    channelName: (state) => (id) => state.channels.find((c) => c.id === id).name || 'N/A',
  },
  mutations: {
    add_pickup: (state, pickup) => state.raid.pickups.push(pickup),
    set_members(state, members) {
      // GM: 981564620764483665
      // Officer: 981564866961768489
      // Champion: 981565176815956008
      // Raider: 981565745697783879
      // Member: 981566511015678022
      // Trial: 981566787923640380
      // Social: 1105942570833555516
      state.members = members.map((m) => {
        const member = { ...m };
        if (member.roles.find((r) => r === '981564620764483665')) {
          // return 'GM';
          member.rank = 100;
        } else if (member.roles.find((r) => r === '981564866961768489')) {
          // return 'Officer';
          member.rank = 90;
        } else if (member.roles.find((r) => r === '981565176815956008')) {
          // return 'Champion';
          member.rank = 80;
        } else if (member.roles.find((r) => r === '981565745697783879')) {
          // return 'Raider';
          member.rank = 70;
        } else if (member.roles.find((r) => r === '981566511015678022')) {
          // return 'Member';
          member.rank = 60;
        } else if (member.roles.find((r) => r === '702187225424592959')) {
          // return 'Member';
          member.rank = 50;
        } else if (member.roles.find((r) => r === '981566787923640380')) {
          // return 'Trial';
          member.rank = 40;
        } else if (member.roles.find((r) => r === '1105942570833555516')) {
          // return 'Social';
          member.rank = 30;
        } else {
          member.rank = 0;
        }
        return member;
      });
    },
    set_raid(state, raid) {
      state.raid = raid;
    },
    set_raids(state, raids) {
      state.raids = raids;
    },
    archive_raid(state, id) {
      state.raids.find((r) => r.id === id).status = 1;
    },
    set_channels(state, channels) {
      state.channels = channels;
    },
  },
  actions: {
    get_channels: ({ commit }) => fetch('/api/v1/channels')
      .then((response) => response.json())
      .then((data) => commit('set_channels', data)),
    get_members: ({ commit }) => fetch('/api/v1/members')
      .then((response) => response.json())
      .then((data) => commit('set_members', data)),
    get_raid: ({ commit }, id) => fetch(`/api/v1/raids/${id}`)
      .then((response) => response.json())
      .then((data) => commit('set_raid', data)),
    get_raids: ({ commit }, id) => fetch('/api/v1/raids')
      .then((response) => response.json())
      .then((data) => commit('set_raids', data)),
    update_pickups: (context, { id, pickups }) => fetch(`/api/v1/raids/${id}/update_pickups`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(pickups),
    }),
    update_signups: ({ commit }, { id, channel, type }) => fetch(`/api/v1/raids/${id}/update_signups`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name: '', channel, type }),
    })
      .then((response) => response.json())
      .then((data) => commit('set_raid', data)),
    delete_raid: ({ commit }, id) => fetch(`/api/v1/raids/${id}`, { method: 'DELETE' })
      .then((response) => response.json())
      .then((data) => commit('set_raids', data)),
    archive_raid: ({ commit }, id) => {
      fetch(`/api/v1/raids/${id}/archive`);
      commit('archive_raid', id);
    },
  },
  modules: {
  },
});
