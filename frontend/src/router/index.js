import Vue from 'vue';
import VueRouter from 'vue-router';
import RaidsView from '../views/RaidsView.vue';
import RaidEditorView from '../views/RaidEditorView.vue';
import MembersView from '../views/MembersView.vue';
import LeaderBoardsView from '../views/LeaderBoardsView.vue';
import PlaceHolderView from '../views/PlaceHolderView.vue';
import MemberDetailsView from '../views/MemberDetailsView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/placeholder',
    name: 'placeholder',
    component: PlaceHolderView,
  },
  {
    path: '/leaderboards',
    name: 'leaderboards',
    component: LeaderBoardsView,
  },
  {
    path: '/',
    name: 'raids',
    component: RaidsView,
  },
  {
    path: '/raids/:id',
    name: 'raidEditor',
    component: RaidEditorView,
  },
  {
    path: '/members',
    name: 'members',
    component: MembersView,
  },
  {
    path: '/members/:id',
    name: 'memberDetails',
    component: MemberDetailsView,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});

export default router;
