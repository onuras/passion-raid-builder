const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '^/api/v1': {
        target: 'http://backend:8000/',
        pathRewrite: { '^/api/v1': '' },
        changeOrigin: true,
      },
    },
  },
});
