FROM node:16
ADD ./frontend /app/
WORKDIR /app
RUN yarn install && yarn build

FROM python:3.10-slim
RUN apt-get update && apt-get install -y nginx-light sqlite3
COPY nginx.conf /etc/nginx/sites-enabled/default
ADD ./backend /app
COPY --from=0 /app/dist /app/dist
WORKDIR /app
RUN pip install -rrequirements.txt
CMD /etc/init.d/nginx start && python -m passion_raid_builder
