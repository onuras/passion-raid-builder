#!/bin/bash
source .venv/bin/activate
exec uvicorn --reload --reload-dir passion_raid_builder --host 0.0.0.0 --port 8000 --root-path /api/v1 passion_raid_builder.api:app
