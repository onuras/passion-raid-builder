import os

DATABASE_PATH = os.environ.get('DATABASE_PATH', 'db.sqlite3')
BOT_TOKEN = os.environ['BOT_TOKEN']
GUILD_ID = os.environ['GUILD_ID']
