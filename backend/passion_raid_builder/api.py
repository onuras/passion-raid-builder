import asyncio
import logging
import json
import os
from datetime import datetime
from fastapi import FastAPI, HTTPException
from typing import List
from pydantic import BaseModel
from . import db
from .signups import Signups
from .config import BOT_TOKEN, GUILD_ID
from .leaderboards import LeaderBoards

app = FastAPI()


class CreateRaidItem(BaseModel):
    name: str
    channel: str
    type: str

class PickupItem(BaseModel):
    id: str
    name: str
    spec: int

class PickupsItem(BaseModel):
    pickups: List[PickupItem]


async def calculate_leaderboards():
    while True:
        now = datetime.utcnow()
        if now.hour == 5 and now.minute == 0:
            try:
                await LeaderBoards().calculate()
            except:
                pass
        await asyncio.sleep(60)


@app.on_event('startup')
async def startup_event():
    await db.connect()
    await db.migrate()
    asyncio.create_task(calculate_leaderboards())

@app.on_event('shutdown')
async def shutdown_event():
    await db.close()

@app.post('/raids/create')
async def create_raid(item: CreateRaidItem):
    signups = await Signups(BOT_TOKEN, GUILD_ID).get_signups(item.channel)
    (id, _m) = await asyncio.gather(
        db.insert_raid(
            item.name,
            item.type,
            item.channel,
            {
                'members': signups['signups'],
                'messages': signups['messages'],
            },
        ),
        db.insert_members(signups['members']),
    )
    return {
        'id': id,
    }

@app.get('/raids')
async def get_raids():
    return await db.get_raids()

@app.get('/raids/{id}')
async def get_raid(id):
    raid = await db.get_raid(id)
    if not raid:
        raise HTTPException(status_code=404, detail='Raid not found')
    return raid

@app.get('/members')
async def get_members():
    return await db.get_members()

@app.post('/raids/{id}/update_pickups')
async def update_pickups(id, items: List[PickupItem]):
    items = map(
        lambda i: {
            'id': i.id,
            'name': i.name,
            'spec': i.spec,
        },
        items
    )
    await db.update_pickups(id, json.dumps(list(items)))
    return 'OK'

@app.post('/raids/{id}/update_signups')
async def update_signups(id, item: CreateRaidItem):
    signups = await Signups(BOT_TOKEN, GUILD_ID).get_signups(item.channel)
    return await db.update_messages(id, {
        'members': signups['signups'],
        'messages': signups['messages'],
    })

@app.delete('/raids/{id}')
async def delete_raid(id):
    await db.delete_raid(id)
    return await get_raids()

@app.get('/raids/{id}/archive')
async def archive_raid(id):
    await db.archive_raid(id)
    return 'OK'


@app.get('/channels')
def channels():
    channels = os.environ.get('CHANNELS')
    if channels:
        return list(
            map(
                lambda c: {
                    'id': c.split('#')[0],
                    'name': '#' + c.split('#')[1]
                },
                os.environ.get('CHANNELS').split(',')
            )
        )

    return {}
