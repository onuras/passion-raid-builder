import json
from datetime import datetime, timedelta, timezone
from . import db


class LeaderBoards:
    def __init__(self):
        self.members = {}

    def new_member(self, id, name):
        self.members[id] = {
            'name': name,
            'pickups': {
                'count': {},
                'roles': {},
            },
            'pickups_last_90_days': {
                'count': {},
                'roles': {},
            },
            'last_raids': []
        }

        for raid_type in ('Total', 'ICC', 'RS', 'TOGC'):
            self.members[id]['pickups'][raid_type] = [0] * 33
            self.members[id]['pickups']['count'][raid_type] = 0
            self.members[id]['pickups']['roles'][raid_type] = {
                'Tank': 0,
                'Healer': 0,
                'MDPS': 0,
                'RDPS': 0,
            };
            self.members[id]['pickups_last_90_days'][raid_type] = [0] * 33
            self.members[id]['pickups_last_90_days']['count'][raid_type] = 0
            self.members[id]['pickups_last_90_days']['roles'][raid_type] = {
                'Tank': 0,
                'Healer': 0,
                'MDPS': 0,
                'RDPS': 0,
            };

    async def calculate(self):
        now = datetime.now(timezone.utc)
        raid_count = int(
            (
                await (
                    await db.conn.execute('SELECT COUNT(*) FROM raids WHERE status = 1')
                ).fetchone()
            )[0]
        )

        for offset in range(0, raid_count+30, 30):
            async with db.conn.execute(
                'SELECT id, modified, type, messages, pickups FROM raids WHERE status = 1 ORDER BY id DESC LIMIT 30 OFFSET ?',
                (offset,)
            ) as cursor:
                async for row in cursor:
                    raid_id = row[0]
                    raid_time = datetime.fromisoformat(row[1])
                    raid_type = row[2]
                    messages = json.loads(row[3])
                    pickups = json.loads(row[4])

                    signups = map(
                        lambda message: {
                            'uid': message['id'],
                            'specs': tuple(set(sum(list(
                                map(
                                    lambda characters: characters['specs'],
                                    message['characters'],
                                ),
                            ), [])))
                        },
                        messages['members'],
                    )

                    for pickup in pickups:
                        if pickup['id'] not in self.members:
                            self.new_member(pickup['id'], pickup['name'])

                        uid = pickup['id']

                        self.members[uid]['pickups'][raid_type][pickup['spec']] += 1
                        self.members[uid]['pickups']['count'][raid_type] += 1
                        self.members[uid]['pickups']['roles'][raid_type][self.role(pickup['spec'])] += 1

                        self.members[uid]['pickups']['Total'][pickup['spec']] += 1
                        self.members[uid]['pickups']['roles']['Total'][self.role(pickup['spec'])] += 1
                        self.members[uid]['pickups']['count']['Total'] += 1

                        if raid_time + timedelta(days=90) > now:
                            self.members[uid]['pickups_last_90_days'][raid_type][pickup['spec']] += 1
                            self.members[uid]['pickups_last_90_days']['count'][raid_type] += 1
                            self.members[uid]['pickups_last_90_days']['roles'][raid_type][self.role(pickup['spec'])] += 1

                            self.members[uid]['pickups_last_90_days']['Total'][pickup['spec']] += 1
                            self.members[uid]['pickups_last_90_days']['roles']['Total'][self.role(pickup['spec'])] += 1
                            self.members[uid]['pickups_last_90_days']['count']['Total'] += 1

                        if len(self.members[uid]['last_raids']) < 10:
                            self.members[uid]['last_raids'].append({
                                'rid': raid_id,
                                'time': row[1],
                                'type': raid_type,
                                'spec': pickup['spec'],
                            })

        for uid in self.members.keys():
            await db.conn.execute(
                'UPDATE members SET pickups = ?, pickups_last_90_days = ?, last_raids = ? WHERE id = ?',
                (
                    json.dumps(self.members[uid]['pickups']),
                    json.dumps(self.members[uid]['pickups_last_90_days']),
                    json.dumps(self.members[uid]['last_raids']),
                    uid,
                )
            )

        await db.conn.commit()

    def role(self, spec):
        if spec in [ 0, 2, 6, 16, 32 ]:
            return 'Tank'
        elif spec in [ 8, 15, 18, 19, 26 ]:
            return 'Healer'
        elif spec in [ 1, 3, 4, 7, 17, 21, 22, 23, 25, 30, 31 ]:
            return 'MDPS'
        else:
            return 'RDPS'
