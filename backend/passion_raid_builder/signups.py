import os
import asyncio
import aiohttp
import json
from .parser import Parser
from .members import Members


class Signups:
    def __init__(self, token, guild_id):
        self.token = token
        self.guild_id = guild_id

    async def _get(self, url, params={}):
        async with aiohttp.ClientSession() as session:
            headers = {
                'Authorization': 'Bot ' + self.token
            }
            async with session.get(
                url,
                params=params,
                headers=headers,
            ) as response:
                return json.loads(await response.text())

    async def get_members(self):
        return Members(
            await self._get(
                'https://discord.com/api/guilds/' + self.guild_id + '/members',
                {'limit': 1000},
            )
        )

    async def get_messages(self, channel_id):
        messages = await self._get(
            'https://discord.com/api/channels/' + channel_id + '/messages'
        )

        while True:
            old_messages = await self._get(
                'https://discord.com/api/channels/' + channel_id + '/messages',
                {'before': messages[-1]['id']}
            )
            messages.extend(old_messages)
            if len(old_messages) == 0:
                break

        return messages

    async def get_signups(self, channel_id):
        (members, messages) = await asyncio.gather(
            self.get_members(),
            self.get_messages(channel_id),
        )
        parse_result = Parser().parse(messages, members)
        return {
            'signups': parse_result['signups'],
            'messages': parse_result['messages'],
            'members': members,
        }
