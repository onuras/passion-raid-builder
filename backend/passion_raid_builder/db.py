import os
import aiosqlite
import sqlite3
import json
import datetime
from .config import DATABASE_PATH

conn = None
_MIGRATIONS = [
    (
        1,
        'Initial database',
        '''
        CREATE TABLE raids (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            status INT DEFAULT 0,
            created DATETIME,
            pulled DATETIME,
            modified DATETIME,
            type TEXT,
            channel TEXT,
            messages TEXT,
            pickups TEXT,
            ratings TEXT
        );

        CREATE TABLE members (
            id VARCHAR(32) PRIMARY KEY,
            roles TEXT,
            username TEXT,
            discriminator TEXT,
            nick TEXT,
            abilities TEXT
        );
        '''
    ),
    (
        2,
        'Add more member columns',
        '''
        ALTER TABLE members ADD COLUMN pickups TEXT;
        ALTER TABLE members ADD COLUMN pickups_last_90_days TEXT;
        ALTER TABLE members ADD COLUMN last_raids TEXT;
        '''
    )
]


async def connect():
    global conn
    conn = await aiosqlite.connect(DATABASE_PATH)

async def close():
    await conn.close()

def now():
    return datetime.datetime.utcnow().replace(
        tzinfo=datetime.timezone.utc
    ).isoformat()

async def _get_database_version():
    try:
        cursor = await conn.execute('SELECT version FROM migrations')
        return (await cursor.fetchone())[0]
    except sqlite3.OperationalError:
        await conn.executescript('''
            CREATE TABLE migrations (version INT);
            INSERT INTO migrations VALUES (0);
        ''')
        return 0

async def migrate():
    database_version = await _get_database_version()
    for (version, description, sql) in _MIGRATIONS:
        if database_version < version:
            await conn.executescript(sql)
            await conn.execute(
                'UPDATE migrations SET version = ?', (version,)
            )
            await conn.commit()

async def insert_members(members):
    members = map(
        lambda member: (
            member['user']['id'],
            json.dumps(member['roles']),
            member['user']['username'],
            member['user']['discriminator'],
            member['nick']
        ),
        members
    )
    await conn.executemany(
        '''
            INSERT INTO members (
              id, roles, username, discriminator, nick
            )
            VALUES (?, ?, ?, ?, ?)
            ON CONFLICT(id)
            DO UPDATE SET
                roles=excluded.roles,
                username=excluded.username,
                discriminator=excluded.discriminator,
                nick=excluded.nick
        ''',
        members
    )
    await conn.commit()

async def insert_raid(name, type_, channel, messages):
    now_ = now()
    id_row = await conn.execute_insert(
        '''INSERT INTO raids (
            name,
            created,
            pulled,
            modified,
            type,
            channel,
            messages,
            pickups
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
        (
            name,
            now_,
            now_,
            now_,
            type_,
            channel,
            json.dumps(messages),
            '[]'
        ),
    )
    await conn.commit()
    return id_row[0]

async def get_raid(id):
    cursor = await conn.execute('SELECT * FROM raids WHERE id = ?', (id,))
    row = await cursor.fetchone()
    return {
        'id': row[0],
        'name': row[1],
        'status': row[2],
        'created': row[3],
        'pulled': row[4],
        'modified': row[5],
        'type': row[6],
        'channel': row[7],
        'signups': json.loads(row[8]) if row[8] else None,
        'pickups': json.loads(row[9]) if row[9] else None,
        'ratings': json.loads(row[10]) if row[10] else None,
    } if row else None


async def archive_raid(id):
    await conn.execute('UPDATE raids SET status = 1 WHERE id = ?', (id,))
    await conn.commit()

async def get_raids():
    cursor = await conn.execute('SELECT id, name, status, created, pulled, modified, type, channel FROM raids ORDER BY id DESC')
    return list(
        map(
            lambda row: {
                'id': row[0],
                'name': row[1],
                'status': row[2],
                'created': row[3],
                'pulled': row[4],
                'modified': row[5],
                'type': row[6],
                'channel': row[7],
            },
            await cursor.fetchall()
        )
    )


async def get_members():
    cursor = await conn.execute('SELECT * FROM members')
    return list(
        map(
            lambda row: {
                'id': row[0],
                'roles': json.loads(row[1]),
                'username': row[2],
                'discriminator': row[3],
                'nick': row[4],
                'abilities': [],
                'pickups': json.loads(row[6]) if row[6] else None,
                'pickups_last_90_days': json.loads(row[7]) if row[7] else None,
                'last_raids': json.loads(row[7]) if row[8] else None,
            },
            await cursor.fetchall()
        )
    )


async def update_pickups(id, pickups):
    await conn.execute(
        'UPDATE raids SET pickups = ?, modified = ? WHERE id = ?',
        (
            pickups,
            now(),
            id
        )
    )
    await conn.commit()

async def update_messages(id, messages):
    await conn.execute(
        'UPDATE raids SET messages = ?, pulled = ? WHERE id = ?',
        (
            json.dumps(messages),
            now(),
            id
        )
    )
    await conn.commit()
    return await get_raid(id)

async def delete_raid(id):
    await conn.execute('DELETE FROM raids WHERE id = ?', (id,))
    await conn.commit()


# Converts integer ids in messages to string.
#
# Discord sends all ids as string but I used parseInt in frontend and that
# messed up every id in pickups. This function fixes id's in pickups.
async def fix_ids():
    members = await get_members()
    raid_count = int(
        (
            await (
                await conn.execute('SELECT COUNT(*) FROM raids')
            ).fetchone()
        )[0]
    )

    for offset in range(0, raid_count+30, 30):
        async with conn.execute(
            'SELECT id, messages, pickups FROM raids ORDER BY id ASC LIMIT 30 OFFSET ?',
            (offset,)
        ) as cursor:
            async for row in cursor:
                raid_id = row[0]
                messages = json.loads(row[1])
                pickups = json.loads(row[2])

                for i, pickup in enumerate(pickups):
                    # This is required to check for inconsistencies
                    possible_members = []
                    for member in members:
                        if round(int(member['id']) / 1000) == round(pickup['id'] / 1000):
                            possible_members.append((i, member))

                    # Checking inconsistencies
                    if len(possible_members) != 1 or (possible_members[0][1]['nick'] != pickup['name'] and possible_members[0][1]['username'] != pickup['name']):
                        print('INCONSISTENCY:')
                        pprint(pickup)
                        pprint(possible_members)
                        print('-------')

                    pickup['id'] = str(possible_members[0][1]['id'])


                for message in messages['members']:
                    message['id'] = str(message['id'])
                    message['mid'] = str(message['mid'])

                for message in messages['messages']:
                    message['id'] = str(message['id'])
                    message['mid'] = str(message['mid'])

                await conn.execute(
                    'UPDATE raids SET messages = ?, pickups = ? WHERE id = ?',
                    (json.dumps(messages), json.dumps(pickups), raid_id)
                )

    await conn.commit()
