class Members(list):
    def __init__(self, *args):
        self._members = args[0]
        super(Members, self).__init__(args[0])

    def find(self, id):
        return next(
            filter(lambda m: m['user']['id'] == id, self._members),
            None
        )

    def nick(self, id):
        member = self.find(id)
        if member:
            return member['nick'] or member['user']['username']
        return None

    def rank(self, id):
        member = self.find(id)
        if not member:
            return -1
        # GM
        elif '981564620764483665' in member['roles']:
            return 100
        # Officer
        elif '981564866961768489' in member['roles']:
            return 90
        # Champion
        elif '981565176815956008' in member['roles']:
            return 80
        # Raider
        elif '981565745697783879' in member['roles']:
            return 70
        # Member
        elif '981566511015678022' in member['roles']:
            return 60
        # Member
        elif '702187225424592959' in member['roles']:
            return 50
        # Trial
        elif '981566787923640380' in member['roles']:
            return 40
        # Initiate
        elif '774713062581731410' in member['roles']:
            return 30
        # Social
        elif '1105942570833555516' in member['roles']:
            return 20
        else:
            return 0
