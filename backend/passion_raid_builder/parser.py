from enum import Enum


class Classes(int, Enum):
    DEATH_KNIGHT = 0
    DRUID = 1
    HUNTER = 2
    MAGE = 3
    PALADIN = 4
    PRIEST = 5
    ROGUE = 6
    SHAMAN = 7
    WARLOCK = 8
    WARRIOR = 9


class Specs(int, Enum):
    DEATH_KNIGHT_BLOOD_TANK = 0
    DEATH_KNIGHT_BLOOD_DPS = 1
    DEATH_KNIGHT_FROST_TANK = 2
    DEATH_KNIGHT_FROST_DPS = 3
    DEATH_KNIGHT_UNHOLY = 4
    DRUID_BALANCE = 5
    DRUID_FERAL_BEAR = 6
    DRUID_FERAL_CAT = 7
    DRUID_RESTORATION = 8
    HUNTER_BEAST_MASTERY = 9
    HUNTER_MARKSMANSHIP = 10
    HUNTER_SURVIVAL = 11
    MAGE_ARCANE = 12
    MAGE_FIRE = 13
    MAGE_FROST = 14
    PALADIN_HOLY = 15
    PALADIN_PROTECTION = 16
    PALADIN_RETRIBUTION = 17
    PRIEST_DISCIPLINE = 18
    PRIEST_HOLY = 19
    PRIEST_SHADOW = 20
    ROGUE_ASSASSINATION = 21
    ROGUE_COMBAT = 22
    ROGUE_SUBTLETY = 23
    SHAMAN_ELEMENTAL = 24
    SHAMAN_ENHANCEMENT = 25
    SHAMAN_RESTORATION = 26
    WARLOCK_AFFLICTION = 27
    WARLOCK_DEMONOLOGY = 28
    WARLOCK_DESTRUCTION = 29
    WARRIOR_ARMS = 30
    WARRIOR_FURY = 31
    WARRIOR_PROTECTION = 32


class MessageParser:
    def __init__(self, message):
        self.message = message.lower().strip()
        self.message_orig = message

    def _parse_class(self):
        classes = {
            Classes.DEATH_KNIGHT: (':_dk:', ':dk:', '(dk)', 'death knight', 'deathknight', 'unholy', 'unh', 'uh'),
            Classes.DRUID: (':_dr:', ':dr:', ':druid:', 'druid', 'bear', 'feral', 'balance', 'bala', 'boomie', 'boomy', 'boomi', 'bomi', 'bumi'),
            Classes.HUNTER: (':_hunt:', ':hunter:', 'hunter', 'marks'),
            Classes.MAGE: (':_ma:', ':mage:', 'mage'),
            Classes.PALADIN: (':_pa:', ':paladin:', 'paladin', 'pala', 'retribution'),
            Classes.PRIEST: (':_pr:', ':priest:', 'priest', 'disc', 'shadow'),
            Classes.ROGUE: (':_rog:', ':rogue:', 'rogue', 'combat', 'assa'),
            Classes.SHAMAN: (':_sh:', ':shaman:', 'shaman', 'enha', 'encha'),
            Classes.WARLOCK: (':_wl:', ':warlock:', 'warlock', 'affli', 'aff', 'demo', 'hybrid', 'destr'),
            Classes.WARRIOR: (':_wa:', ':warrior:', 'warrior', 'fury', 'arms'),
        }
        for class_ in classes.keys():
            for substr in classes[class_]:
                if substr in self.message:
                    self.class_ = class_
                    return class_
        return None

    def _parse_spec(self, cls):
        classes = {
            Classes.DEATH_KNIGHT: {
                Specs.DEATH_KNIGHT_BLOOD_DPS: ('blood dps',),
                Specs.DEATH_KNIGHT_BLOOD_TANK: ('blood', 'tank'),
                Specs.DEATH_KNIGHT_FROST_TANK: ('frost tank',),
                Specs.DEATH_KNIGHT_FROST_DPS: ('frost', 'fdk'),
                Specs.DEATH_KNIGHT_UNHOLY: ('unholy', 'unh', 'uh'),
            },
            Classes.DRUID: {
                Specs.DRUID_BALANCE: ('balance', 'bala', 'boomie', 'boomy', 'boomi', 'bomi', 'bumi'),
                Specs.DRUID_FERAL_BEAR: ('bear', 'tank'),
                Specs.DRUID_FERAL_CAT: ('cat', 'feral'),
                Specs.DRUID_RESTORATION: ('resto', 'tree'),
            },
            Classes.HUNTER: {
                Specs.HUNTER_MARKSMANSHIP: ('mm', 'marks'),
                Specs.HUNTER_BEAST_MASTERY: ('bm', 'beast'),
                Specs.HUNTER_SURVIVAL: ('survival', 'surv'),
            },
            Classes.MAGE: {
                Specs.MAGE_FIRE: ('fire', 'ttw'),
                Specs.MAGE_ARCANE: ('arcane', 'arc'),
                Specs.MAGE_FROST: ('frost',),
            },
            Classes.PALADIN: {
                Specs.PALADIN_HOLY: ('holy', 'heal', 'hpal'),
                Specs.PALADIN_PROTECTION: ('prot', 'tank'),
                Specs.PALADIN_RETRIBUTION: ('retribution', 'ret', 'dps', 're3'),
            },
            Classes.PRIEST: {
                Specs.PRIEST_DISCIPLINE: ('disc',),
                Specs.PRIEST_HOLY: ('holy',),
                Specs.PRIEST_SHADOW: ('shadow', 'dps', ' sp'),
            },
            Classes.ROGUE: {
                Specs.ROGUE_COMBAT: ('combat',),
                Specs.ROGUE_ASSASSINATION: ('assa',),
                Specs.ROGUE_SUBTLETY: ('sub',),
            },
            Classes.SHAMAN: {
                Specs.SHAMAN_RESTORATION: ('resto', 'heal', 'rsham'),
                Specs.SHAMAN_ENHANCEMENT: ('enha', 'encha', 'enh'),
                Specs.SHAMAN_ELEMENTAL: ('ele',),
            },
            Classes.WARLOCK: {
                Specs.WARLOCK_AFFLICTION: ('affli', 'aff'),
                Specs.WARLOCK_DEMONOLOGY: ('demo', 'hybrid'),
                Specs.WARLOCK_DESTRUCTION: ('dest', 'desmo'),
            },
            Classes.WARRIOR: {
                Specs.WARRIOR_FURY: ('fury', 'dps'),
                Specs.WARRIOR_PROTECTION: ('prot', 'tank'),
                Specs.WARRIOR_ARMS: ('arms', 'arm'),
            },
        }
        for class_ in classes:
            if cls != class_:
                continue
            for spec in classes[class_]:
                for keyword in classes[class_][spec]:
                    if keyword in self.message:
                        return (spec, keyword)
        return (None, None)

    def _parse_icc_preference(self):
        if ':lodp:' in self.message or ':star:' in self.message or \
                '\u2705' in self.message:
            return True
        return False

    def _parse_rs_preference(self):
        if ':rsp:' in self.message or ':star:' in self.message or \
                '\u2705' in self.message:
            return True
        return False

    def _parse_togc_preference(self):
        if ':togcp:' in self.message:
            return True
        return False

    def _parse_icc_availability(self):
        if ':lodx:' in self.message or '\u274c' in self.message or \
                self.message.startswith('~~'):
            return False
        return True

    def _parse_rs_availability(self):
        if ':rsx:' in self.message or '\u274c' in self.message or \
                self.message.startswith('~~'):
            return False
        return True

    def _parse_togc_availability(self):
        if ':togcx:' in self.message:
            return False
        return True

    def _parse_shards(self):
        for substr in ['shard', 'sfs']:
            if substr in self.message:
                return True
        return False

    def parse(self):
        class_ = self._parse_class()
        if class_ is None:
            return None

        specs = []
        for _ in range(3):
            (spec, substr) = self._parse_spec(class_)
            if spec != None:
                if substr:
                    self.message = self.message.replace(substr, '')
                if spec not in specs:
                    specs.append(spec)

        if class_ == Classes.HUNTER and len(specs) == 0:
            specs.append(Specs.HUNTER_MARKSMANSHIP)
        elif class_ == Classes.MAGE and len(specs) == 0:
            specs.append(Specs.MAGE_FIRE)
        elif class_ == Classes.ROGUE and len(specs) == 0:
            specs.append(Specs.ROGUE_COMBAT)

        result = {
            'class': class_,
            'specs': specs,
            'preferred': {
                'ICC': self._parse_icc_preference(),
                'RS': self._parse_rs_preference(),
                'TOGC': self._parse_togc_preference(),
            },
            'availability': {
                'ICC': self._parse_icc_availability(),
                'RS': self._parse_rs_availability(),
                'TOGC': self._parse_togc_availability(),
            },
            'shards': self._parse_shards(),
        }
        return result


class Parser:
    def parse(self, messages, members):
        characters = []
        failed_messages = []

        for message in messages:
            if message['pinned']:
                continue

            passion_parser = {
                'chars': [],
                'failure': False,
                'failed_lines': [],
            }

            chars = []

            for message_content in message['content'].split('\n'):
                message_result = MessageParser(message_content).parse()
                if message_result:
                    chars.append(message_result)
                elif message_content != '':
                    passion_parser['failure'] = True
                    passion_parser['failed_lines'].append(message_content)

            if len(chars) > 0:
                passion_parser['chars'] = chars
            else:
                passion_parser['failure'] = True
                passion_parser['failed_lines'] = [ message['content'] ]

            (nickname, rank) = (
                members.nick(message['author']['id']),
                members.rank(message['author']['id']),
            )

            characters.append({
                'id': message['author']['id'],
                'mid': message['id'],
                'name': nickname,
                'rank': rank,
                'characters': passion_parser['chars'],
                'message': message['content'],
            })

            if passion_parser['failure']:
                failed_messages.append({
                    'id': message['author']['id'],
                    'mid': message['id'],
                    'name': nickname,
                    'rank': rank,
                    'content': '\n'.join(passion_parser['failed_lines']),
                })

        failed_messages.reverse()
        return {
            'signups': characters,
            'messages': failed_messages,
        }
