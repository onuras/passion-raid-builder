import asyncio
from sys import argv
import uvicorn
from . import db
from .leaderboards import LeaderBoards

async def calculate_leaderboards():
    await db.connect()
    await LeaderBoards().calculate()
    await db.close()

async def fix_ids():
    await db.connect()
    await db.fix_ids()
    await db.close()

async def db_migrate():
    await db.connect()
    await db.migrate()
    await db.close()


if __name__ == "__main__":
    if len(argv) > 1 and argv[1] == '--calculate-leaderboards':
        asyncio.run(calculate_leaderboards())
    elif len(argv) > 1 and argv[1] == '--fix-ids':
        asyncio.run(fix_ids())
    elif len(argv) > 1 and argv[1] == '--db-migrate':
        asyncio.run(db_migrate())
    else:
        uvicorn.run(
            "passion_raid_builder.api:app",
            port=8000,
            log_level="info",
            root_path="/api/v1",
        )
